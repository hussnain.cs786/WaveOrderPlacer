package com.waveorderplacer;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.waveorderplacer.fragment.ProductsFragment;
import com.waveorderplacer.model.User;

/**
 * Created by ranatayyab on 2/11/18.
 */

public class WOP extends Application {
    public static User u = null;
    public static String qrcode = null;
    public static String cat = null;
    public static ProductsFragment p = null;

    /**
     * @return
     */
    public static FirebaseAuth getAuth() {
        return FirebaseAuth.getInstance();
    }

    /**
     * @return
     */
    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    /**
     * @return
     */
    public static Boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    /**
     * @return
     */
    public static DatabaseReference getDatabaseRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    /**
     * @return
     */
    public static StorageReference getStorageRef() {
        return FirebaseStorage.getInstance().getReference();
    }

    /**
     * @param task
     * @return
     */
    public static UploadTask getUploadTask(UploadTask task) {
        return task;
    }

}
