package com.waveorderplacer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.model.User;

import java.io.IOException;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateAccountActivity extends AppCompatActivity {

    private final int PICK_IMAGE_REQUEST = 71;
    @BindView(R.id.fname)
    EditText mFName;
    @BindView(R.id.lname)
    EditText mLName;
    @BindView(R.id.company_shop_name)
    EditText mCompanyShopName;
    @BindView(R.id.contact)
    EditText mContact;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.password)
    EditText mPassword;
    @BindView(R.id.country)
    EditText mCountry;
    @BindView(R.id.city)
    EditText mCity;
    @BindView(R.id.town_colony_market)
    EditText mTownCityMarket;
    @BindView(R.id.street_number)
    EditText mStreetNo;
    @BindView(R.id.shop_plot_number)
    EditText mShopPlotNumber;
    @BindView(R.id.display_image)
    ImageView display_image;
    String image, image_thumb;
    String type;

    // Storage Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    private Uri filepath;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);

        mProgressDialog = new ProgressDialog(this);

        type = getIntent().getStringExtra("type");

        if (type == null)
            type = "retailer";


        /*Picasso.with(CreateAccountActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.default_avatar).into(display_image, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

                Picasso.with(CreateAccountActivity.this).load(image).placeholder(R.drawable.default_avatar).into(display_image);

            }
        });*/


    }


    @OnClick(R.id.select_image)
    public void UploadImage(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST) {

            filepath = data.getData();
            Log.e("Path", filepath.toString());
            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),filepath);
                display_image.setImageBitmap(bitmap);
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

    }




    @OnClick(R.id.register)
    public void register(View v) {
        final String fn = mFName.getText().toString();
        final String ln = mLName.getText().toString();
        final String csn = mCompanyShopName.getText().toString();
        final String cnt = mContact.getText().toString();
        final String ctry = mCountry.getText().toString();
        final String cty = mCity.getText().toString();
        final String tcm = mTownCityMarket.getText().toString();
        final String stn = mStreetNo.getText().toString();
        final String spn = mShopPlotNumber.getText().toString();
        final String e = mEmail.getText().toString();
        final String p = mPassword.getText().toString();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());


        if (fn.isEmpty() || ln.isEmpty() || csn.isEmpty() || cnt.isEmpty() || ctry.isEmpty()
                || cty.isEmpty()
                || tcm.isEmpty()
                || stn.isEmpty()
                || spn.isEmpty()
                || e.isEmpty()
                || p.isEmpty()
                ) {
            Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_LONG)
                    .show();
        } else{

            mProgressDialog.setTitle("Signing Up");
            mProgressDialog.setMessage("Please wait while we check your credentials.");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final String pic = String.valueOf(taskSnapshot.getDownloadUrl());
                    WOP.getAuth().createUserWithEmailAndPassword(e, p)
                            .addOnCompleteListener(CreateAccountActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("Login", "signInWithEmail:success");
                                        User user = new User(WOP.getUserId(),
                                                type,
                                                fn,
                                                ln,
                                                cnt,
                                                ctry,
                                                cty,
                                                csn,
                                                e,
                                                tcm,
                                                stn,
                                                spn,
                                                pic);
                                        WOP.getDatabaseRef().child("users")
                                                .child(WOP.getUserId()).setValue(user);
                                        WOP.u = user;
                                        startActivity(new Intent(CreateAccountActivity.this, MainActivity.class));
                                        mProgressDialog.dismiss();


                                    } else {
                                        // If sign in fails, display a message to the user.
                                        mProgressDialog.hide();
                                        Log.w("Login", "signInWithEmail:failure", task.getException());
                                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    }

                                    // ...
                                }
                            });
                }
            });
        }
    }
}
