package com.waveorderplacer.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.model.Product;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateProductActivity extends AppCompatActivity {

    private final int PICK_IMAGE_REQUEST = 71;
    @BindView(R.id.qrcode)
    TextView qr_scan;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.company)
    EditText company;
    @BindView(R.id.category)
    Spinner category;
    @BindView(R.id.size)
    EditText size;
    @BindView(R.id.price)
    EditText price;
    String result, ctgry;
    // Storage Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    private Uri filepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_create_product);

        ButterKnife.bind(this);

        setTitle("ADD NEW PRODUCT");

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ctgry = parent.getItemAtPosition(position).toString();
                Toast.makeText(CreateProductActivity.this, ctgry, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @OnClick(R.id.scan_qr)
    public void scanQRCode(View v)
    {


        Permissions.check(this, new String[]{android.Manifest.permission.CAMERA},
                "Camera and storage permissions are required because...",
                new Permissions.Options()
                        .setSettingsDialogTitle("Warning!").setRationaleDialogTitle("Info"),
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        startActivityForResult(new Intent(CreateProductActivity.this, ScanQRCodeActivity.class), 1);
                    }
                });

    }

    @OnClick(R.id.select_image)
    public void UploadImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                result = data.getStringExtra("result");
                qr_scan.setText(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getApplicationContext(), "You have canceled the scan.", Toast.LENGTH_LONG)
                        .show();
            }

        }
        if (requestCode == PICK_IMAGE_REQUEST) {

            filepath = data.getData();
        }
    }


    @OnClick(R.id.add_products)
    public void AddProducts(View v){
        final String n = name.getText().toString();
        final String cmp = company.getText().toString();
        final String s = size.getText().toString();
        final String p = price.getText().toString();
        final String r = result;
        final String c = ctgry;

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());


        if (n.isEmpty()
                || cmp.isEmpty()
                || s.isEmpty()
                || p.isEmpty()
                || r.isEmpty()
                || c.isEmpty()) {
            Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_LONG)
                    .show();
        } else {

            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final String pic = taskSnapshot.getDownloadUrl().toString();

                    String key = WOP.getDatabaseRef().child("products").push().getKey();
                    Product productValues = new Product(key, r, n, cmp, c, s, p, pic);

                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/products/" + key, productValues);
                    childUpdates.put("/user-products/" + WOP.getUserId() + "/" + key, productValues);

                    WOP.getDatabaseRef().updateChildren(childUpdates);
                    Toast.makeText(getApplicationContext(), "Product added successfully.", Toast.LENGTH_LONG)
                            .show();
                    finish();

                }
            });




        }

    }


}
