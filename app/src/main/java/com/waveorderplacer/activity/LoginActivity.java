package com.waveorderplacer.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.email)
    EditText mEmail;

    @BindView(R.id.password)
    EditText mPassword;

    private ProgressDialog mLoginProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginProgress = new ProgressDialog(this);

    }

    @OnClick(R.id.sign_in)
    public void signIn(View v) {
        String e = mEmail.getText().toString();
        String p = mPassword.getText().toString();

        if (e.isEmpty() || p.isEmpty()) {
            Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_LONG)
                    .show();
        } else {

            mLoginProgress.setTitle("Logging In");
            mLoginProgress.setMessage("Please wait while we check your credentials.");
            mLoginProgress.setCanceledOnTouchOutside(false);
            mLoginProgress.show();

            WOP.getAuth().signInWithEmailAndPassword(e, p)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                WOP.getDatabaseRef()
                                        .child("users")
                                        .child(WOP.getUserId())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                Log.e("OK", "OK");
                                                User u = dataSnapshot.getValue(User.class);
                                                if (u != null) {
                                                    WOP.u = u;
                                                    mLoginProgress.dismiss();
                                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again.", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                Log.e("Error", databaseError.toException().getMessage());
                                            }
                                        });

                                Log.d("Login", "signInWithEmail:success");

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("Login", "signInWithEmail:failure", task.getException());
                                mLoginProgress.hide();
                                Toast.makeText(LoginActivity.this, "Error : " + task.getException().getMessage(), Toast.LENGTH_LONG).show();


                            }

                            // ...
                        }
                    });
        }

    }

    @OnClick(R.id.create_new_account)
    public void createNewAccount(View v) {
        final Intent intent = new Intent(LoginActivity.this, CreateAccountActivity.class);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Are you?");
        alert.setMessage("Retailer or Supplier");

        alert.setIcon(android.R.drawable.ic_dialog_info);

        alert.setPositiveButton("I'm Retailer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                intent.putExtra("type", "retailer");
                startActivity(intent);
            }
        });

        alert.setNegativeButton("I'm Supplier", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                intent.putExtra("type", "supplier");
                startActivity(intent);
            }
        });

        alert.show();


    }
}
