package com.waveorderplacer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.adapter.PageAdapter;
import com.waveorderplacer.fragment.ProductsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import android.support.v4.widget.*;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    FragmentTransaction ft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (WOP.u.userType.equals("supplier")) {
            mTabLayout.addTab(mTabLayout.newTab().setText("My Products"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Notification"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Profile"));
        }else {
            mTabLayout.addTab(mTabLayout.newTab().setText("Products"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Notification"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Profile"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Cart"));
            mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        final PagerAdapter adapter = new PageAdapter( getSupportFragmentManager(),mTabLayout.getTabCount());

        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (WOP.u.userType.equals("supplier")) {

                    mViewPager.setCurrentItem(tab.getPosition());
                    switch (tab.getPosition()) {
                        case 0:
                                setTitle("Products");
                            break;
                        case 1:
                            setTitle("Notifications");
                            WOP.cat = null;
                            break;
                        case 2:
                            WOP.cat = null;
                            setTitle("Profile");
                            break;
                    }
                }
                else {
                    mViewPager.setCurrentItem(tab.getPosition());
                    switch (tab.getPosition()) {
                        case 0:
                                setTitle("Products");
                            break;
                        case 1:
                            WOP.cat = null;
                            setTitle("Notifications");
                            break;
                        case 2:
                            WOP.cat = null;
                            setTitle("Profile");
                            break;
                        case 3:
                            WOP.cat = null;
                            setTitle("Cart");
                            break;
                }

            }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View v = navigationView.getHeaderView(0);

        TextView name = v.findViewById(R.id.header_name);
        TextView email = v.findViewById(R.id.header_email);
        CircularImageView imageView = v.findViewById(R.id.header_image);

        name.setText(String.valueOf(WOP.u.fname + " " + WOP.u.lname)+" ("+WOP.u.userType+")");
        email.setText(WOP.u.email);

        Picasso.with(this).load(WOP.u.pic).placeholder(R.drawable.iconw).into(imageView);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       //
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:

                WOP.getAuth().signOut();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){
            case R.id.nav_products:
                WOP.qrcode = null;
                WOP.cat = null;
                mViewPager.setCurrentItem(0);
                ft = getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();
                break;
            case R.id.nav_notifications:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.nav_profile:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.nav_logout:

                WOP.getAuth().signOut();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();
                break;
            case R.id.nav_food:
                WOP.cat = "food";
                mViewPager.setCurrentItem(0);
                ft = getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();

                break;
            case R.id.nav_beauty:
                WOP.cat = "beauty";
                mViewPager.setCurrentItem(0);
                ft = getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();
                break;
            case R.id.nav_household:
                WOP.cat = "house";

                Log.e("ABC", String.valueOf(WOP.p));
                mViewPager.setCurrentItem(0);
                ft = getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();
                break;
           /* case R.id.all_products:
                WOP.cat = null;
                mViewPager.setCurrentItem(0);
                ft = getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();
                break;*/
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
