package com.waveorderplacer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.image_view)
    ImageView mImageView;

    @BindView(R.id.text_1)
    TextView mText1;

    @BindView(R.id.text_2)
    TextView mText2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.mytransition);

        mImageView.startAnimation(myanim);
        mText1.startAnimation(myanim);
        mText2.startAnimation(myanim);


        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (WOP.isSignedIn()) {
                    Log.e("isSignIn", "User is signed in " + WOP.getUserId());
                    WOP.getDatabaseRef()
                            .child("users")
                            .child(WOP.getUserId())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Log.e("OK", "OK");
                                    User u = dataSnapshot.getValue(User.class);
                                    if (u != null) {
                                        WOP.u = u;
                                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                        // close this activity
                                        finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Something went wrong please try again.", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("Error", databaseError.toException().getMessage());
                                }
                            });
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    // close this activity
                    finish();
                }


            }
        }, SPLASH_TIME_OUT);




    }


}
