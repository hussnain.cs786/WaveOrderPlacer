package com.waveorderplacer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.waveorderplacer.WOP;
import com.waveorderplacer.fragment.CartFragment;
import com.waveorderplacer.fragment.NotificationsFragment;
import com.waveorderplacer.fragment.ProductsFragment;
import com.waveorderplacer.fragment.ProfileFragment;

/**
 * Created by hussnain on 2/11/18.
 */

public class PageAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;


    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

    }

    @Override
    public Fragment getItem(int position) {

        if (WOP.u.userType.equals("supplier")){
            switch (position) {
                case 0:
                    WOP.p = new ProductsFragment();
                    return WOP.p;
                case 1:
                    return new NotificationsFragment();
                case 2:
                    return new ProfileFragment();

                default:
                    return null;
            }
        }
        else {

            switch (position) {
                case 0:
                    return WOP.p = new ProductsFragment();
                case 1:
                    return new NotificationsFragment();
                case 2:
                    return new ProfileFragment();
                case 3:
                    return new CartFragment();


                default:
                    return null;
            }

        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
