package com.waveorderplacer.adapter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.activity.MainActivity;
import com.waveorderplacer.model.Cart;
import com.waveorderplacer.model.Notification;
import com.waveorderplacer.model.Product;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_PRODUCTS = 0;
    public final static int VIEW_TYPE_NOTIFICATIONS = 1;
    public final static int VIEW_TYPE_CART = 2;
    public static final int uniqueID = 123;
    private final List<Object> mValues;
    private final Context mCtx;
    @ListViewType private final int mListViewType;
    NotificationCompat.Builder notification;
    String payment_method;

    public RecyclerViewAdapter(@ListViewType int listViewType, List<Object> items, Context ctx) {
        mListViewType = listViewType;
        mValues = items;
        mCtx = ctx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, @ListViewType int viewType) {
        switch (getListViewType()){
            case VIEW_TYPE_PRODUCTS:
                return new ProductHolder(LayoutInflater.from(parent.getContext()), parent);

            case VIEW_TYPE_NOTIFICATIONS:
                return new NotificationHolder(LayoutInflater.from(parent.getContext()), parent);
            case VIEW_TYPE_CART:
                return new CartHolder(LayoutInflater.from(parent.getContext()), parent);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (getListViewType()) {
            case VIEW_TYPE_PRODUCTS:
                final ProductHolder holder1 = (ProductHolder) holder;

                holder1.product = (Product) mValues.get(position);
                holder1.init();

                notification = new NotificationCompat.Builder(mCtx);
                notification.setAutoCancel(true);

                holder1.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mCtx, String.valueOf(holder1.product.name), Toast.LENGTH_SHORT).show();
                        // Toast.makeText(mCtx, String.valueOf(WOP.u.userType), Toast.LENGTH_SHORT).show();

                        if (WOP.u.userType.equals( "retailer")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mCtx);
                        //LayoutInflater inflater = mCtx.getLayoutInflater();
                        LayoutInflater inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
                        dialogBuilder.setView(dialogView);

                            final EditText editText = dialogView.findViewById(R.id.quantity);
                            final Button pay_on_delivery = dialogView.findViewById(R.id.cash_on_delivery);
                            final Button online_payment = dialogView.findViewById(R.id.online_payment);
                            final SegmentedButtonGroup segmentedButtonGroup = dialogView.findViewById(R.id.segmentedButtonGroup);
                            segmentedButtonGroup.setOnClickedButtonPosition(new SegmentedButtonGroup.OnClickedButtonPosition() {
                                @Override
                                public void onClickedButtonPosition(int position) {
                                    switch (position)
                                    {
                                        case 0:
                                            payment_method = "cash on delivery";
                                            break;
                                        case 1:
                                            payment_method = "online payment";
                                            break;
                                    }
                                }
                            });


                        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                String product_name = holder1.product.name;
                                String product_size = editText.getText().toString();
                                String product_category = holder1.product.category;
                                String pay_method = payment_method;

                                if (!product_name.isEmpty()
                                        || !product_size.isEmpty()
                                        || !product_category.isEmpty()
                                        || !pay_method.isEmpty()
                                        ) {

                                    String key = WOP.getDatabaseRef().child("notifications").push().getKey();
                                    Cart cartValues = new Cart(key, product_name, product_category, product_size,pay_method);

                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("/cart/" + key, cartValues);
                                    childUpdates.put("/user-cart/" + WOP.getUserId() + "/" + key, cartValues);

                                    WOP.getDatabaseRef().updateChildren(childUpdates);
                                    // Toast.makeText(mCtx, "Notification sent.", Toast.LENGTH_LONG).show();


                                }

                            }
                        });

                        AlertDialog b = dialogBuilder.create();
                        b.show();

                    }


                    }
                });

                break;
            case VIEW_TYPE_NOTIFICATIONS:
                final NotificationHolder holder2 = (NotificationHolder) holder;

                holder2.notification = (Notification) mValues.get(position);

                holder2.l1.setVisibility(View.GONE);
                holder2.l2.setVisibility(View.GONE);
                holder2.init();

/*
                if (WOP.u.userType.equals("supplier")) {

                    holder2.cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss a");
                            String noti_time = df.format(c.getTime());

                            String name = WOP.u.fname + " " + WOP.u.lname;
                            String message = "Thank You! Your order received";
                            String cell = WOP.u.contact;
                            String product_quantity = holder2.notification.product_quantity;
                            String productname = holder2.notification.product_name;

                            if (WOP.u.userType.equals("supplier")) {
                                notification.setSmallIcon(R.drawable.logo);
                                notification.setWhen(System.currentTimeMillis());
                                notification.setContentTitle("Dear " + name);
                                notification.setPriority(NotificationCompat.PRIORITY_HIGH);
                                notification.setContentText(message);

                                Intent intent = new Intent(mCtx, MainActivity.class);
                                PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0,
                                        intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                notification.setContentIntent(pendingIntent);

                                //Builds notifications and issues it
                                NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
                                //
                                notificationManager.notify(uniqueID, notification.build());


                                if (!productname.isEmpty()
                                        || !product_quantity.isEmpty()
                                        || !message.isEmpty()
                                        || !noti_time.isEmpty()
                                        || !name.isEmpty()
                                        | !cell.isEmpty()) {

                                    String key = WOP.getDatabaseRef().child("notifications").push().getKey();
                                    Notification notificationValues = new Notification(name, message, productname, product_quantity, noti_time, cell);

                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("/notifications/" + key, notificationValues);
                                    childUpdates.put("/user-notifications/" + WOP.getUserId() + "/" + key, notificationValues);

                                    WOP.getDatabaseRef().updateChildren(childUpdates);
                                }
                            }

                        }
                    });
                }*/


                break;
            case VIEW_TYPE_CART:

                final CartHolder holder3 = (CartHolder) holder;

                holder3.cart = (Cart) mValues.get(position);
                holder3.init();

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @ListViewType
    public int getListViewType(){return mListViewType;}

    @IntDef({VIEW_TYPE_PRODUCTS, VIEW_TYPE_NOTIFICATIONS, VIEW_TYPE_CART})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ListViewType {
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        public Product product;
        @BindView(R.id.pName)
        TextView product_name;
        @BindView(R.id.cName)
        TextView company_name;
        @BindView(R.id.itemCategory)
        TextView item_category;
        @BindView(R.id.itemSize)
        TextView item_size;
        @BindView(R.id.itemPrice)
        TextView item_price;

        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.product_image)
        CircularImageView mImage;

        public ProductHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_product,parent,false));

            ButterKnife.bind(this,itemView);

        }

        public void init() {
            product_name.setText(product.name);
            company_name.setText(product.company);
            item_category.setText(product.category);
            item_size.setText(product.size);
            item_price.setText(product.price);
            Picasso.with(mCtx).load(product.pic).placeholder(R.drawable.default_avatar).into(mImage);
        }


    }

    public class NotificationHolder extends RecyclerView.ViewHolder {

        public Notification notification;

        @Nullable
        @BindView(R.id.notiSender)
        TextView sender_name;

        @Nullable
        @BindView(R.id.notiMessage)
        TextView message;

        @Nullable
        @BindView(R.id.p_name)
        TextView p_name;

        @Nullable
        @BindView(R.id.p_size)
        TextView p_size;

        @Nullable
        @BindView(R.id.noti_time)
        TextView notification_time;

        @Nullable
        @BindView(R.id.p_cell_no)
        TextView phone_num;

        @Nullable
        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.l1)
        LinearLayout l1;

        @BindView(R.id.l2)
        LinearLayout l2;

        public NotificationHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_notification,parent,false));


            ButterKnife.bind(this,itemView);

        }

        public void init() {
            sender_name.setText(notification.name);
            message.setText(notification.message);
            p_name.setText(notification.product_name);
            p_size.setText(notification.product_quantity);
            notification_time.setText(notification.notification_time);
            phone_num.setText(notification.cell_no);
        }


    }

    public class CartHolder extends RecyclerView.ViewHolder {
        public Cart cart;

        public Product product;

        @BindView(R.id.p_name_)
        TextView product_name_;

        @BindView(R.id.item_category_)
        TextView item_category_;

        @BindView(R.id.item_size_)
        TextView item_size_;


        @BindView(R.id.cardView)
        CardView cardView;

        public CartHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_cart,parent,false));

            ButterKnife.bind(this,itemView);

        }

        public void init() {
            product_name_.setText(cart.name);
            item_category_.setText(cart.category);
            item_size_.setText(cart.size);
        }


    }


    }

