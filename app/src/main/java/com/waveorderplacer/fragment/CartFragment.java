package com.waveorderplacer.fragment;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.activity.MainActivity;
import com.waveorderplacer.adapter.RecyclerViewAdapter;
import com.waveorderplacer.model.Cart;
import com.waveorderplacer.model.Notification;
import com.waveorderplacer.model.Product;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.waveorderplacer.adapter.RecyclerViewAdapter.uniqueID;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {


    @BindView(R.id.list)
    RecyclerView mRecyclerView;

    DatabaseReference ref;

    String product_name;
    String product_size;

    int s;

    NotificationCompat.Builder notification;



    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart_list, container, false);
        ButterKnife.bind(this, view);

        Context context = view.getContext();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        notification  = new NotificationCompat.Builder(getContext());
        notification.setAutoCancel(true);

        ref = WOP.getDatabaseRef().child("user-cart").child(WOP.getUserId());



        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Carts", "OK");
                final List<Object> list = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Cart c = dataSnapshot1.getValue(Cart.class);
                    list.add(c);
                }
                mRecyclerView.setAdapter(new RecyclerViewAdapter(RecyclerViewAdapter.VIEW_TYPE_CART, list, getContext()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ErrorCart", databaseError.toException().getMessage());
            }
        });

        return view;
    }

    @OnClick(R.id.order)
    public void OrderNow() {

//        if (WOP.u.userType.equals("retailer")) {

            WOP.getDatabaseRef().child("user-cart").child(WOP.getUserId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        product_name = String.valueOf(dataSnapshot1.child("name").getValue());
                        product_size = String.valueOf(dataSnapshot1.child("size").getValue());

                        s = product_name.length();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            String name = WOP.u.fname + " " + WOP.u.lname;
            String message = "I'm interested to bought some items.";
            String contact_me = WOP.u.contact;
            String pn = String.valueOf(s);
            String ps = product_size;


        /*Log.e("Data",ps);
        Log.e("Data1",message);
        Log.e("Data3",name);
        Log.e("Data4",contact_me);*/

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss a");
            String noti_time = df.format(c.getTime());

//            if (WOP.u.userType.equals("supplier")) {
                notification.setSmallIcon(R.drawable.logo);
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle(name);
                notification.setPriority(NotificationCompat.PRIORITY_HIGH);
                notification.setContentText(message);

                Intent intent = new Intent(getContext(), MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //Builds notifications and issues it
                NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(uniqueID, notification.build());



                if (!pn.isEmpty()
                        || !ps.isEmpty()
                        || !message.isEmpty()
                        || !noti_time.isEmpty()
                        || !name.isEmpty()
                        | !contact_me.isEmpty()) {



                    String key = WOP.getDatabaseRef().child("notifications").push().getKey();
                    Notification notificationValues = new Notification(name, message, product_name, product_size, noti_time, contact_me);

                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/notifications/" + key, notificationValues);
                    childUpdates.put("/user-notifications/" + WOP.getUserId() + "/" + key, notificationValues);

                    WOP.getDatabaseRef().updateChildren(childUpdates);
                    // Toast.makeText(mCtx, "Notification sent.", Toast.LENGTH_LONG).show();


                }
//            }

            Log.e("Sting", String.valueOf(WOP.getDatabaseRef().orderByValue()));

            WOP.getDatabaseRef().child("user-cart").removeValue();
            WOP.getDatabaseRef().child("cart").removeValue();

        }
    }

//}
