package com.waveorderplacer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.adapter.RecyclerViewAdapter;
import com.waveorderplacer.model.Notification;
import com.waveorderplacer.model.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView mRecyclerView;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NotificationsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_list, container, false);

        ButterKnife.bind(this, view);
        // Set the adapter
            Context context = view.getContext();
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            DatabaseReference ref = WOP.getDatabaseRef().child("notifications");

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.e("Products", "OK");
                    final List<Object> list = new ArrayList<>();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Notification n = dataSnapshot1.getValue(Notification.class);
                        list.add(n);
                    }
                    mRecyclerView.setAdapter(new RecyclerViewAdapter(RecyclerViewAdapter.VIEW_TYPE_NOTIFICATIONS, list, getContext()));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("ErrorProduct", databaseError.toException().getMessage());
                }
            });
        return view;
    }



}
