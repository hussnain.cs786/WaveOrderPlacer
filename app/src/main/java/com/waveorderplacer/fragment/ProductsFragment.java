package com.waveorderplacer.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.activity.CreateProductActivity;
import com.waveorderplacer.activity.ScanQRCodeActivity;
import com.waveorderplacer.adapter.RecyclerViewAdapter;
import com.waveorderplacer.model.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A fragment representing a list of Items.
 */
public class ProductsFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView mRecyclerView;

    @BindView(R.id.floating_action_button)
    FloatingActionButton mAddProduct;



    public ProductsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        ButterKnife.bind(this, view);

        Context context = view.getContext();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        DatabaseReference ref = WOP.getDatabaseRef().child("user-products").child(WOP.getUserId());

        if (WOP.u.userType.equals("retailer")) {
            ref = WOP.getDatabaseRef().child("products");

        }


        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Products", "OK");
                final List<Object> list = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Product p = dataSnapshot1.getValue(Product.class);
                    Log.e("Code", WOP.qrcode + ", " + p.qrcode);
                    if (WOP.qrcode != null && p.qrcode.toLowerCase().contains(WOP.qrcode))
                    {
                        list.add(p);
                        Log.e("Code", WOP.qrcode + ", " + p.qrcode);
                    }
                    else if (WOP.cat != null && p.category.toLowerCase().contains(WOP.cat))
                        list.add(p);
                    else if (WOP.cat == null && WOP.qrcode == null)
                        list.add(p);

                }
                mRecyclerView.setAdapter(new RecyclerViewAdapter(RecyclerViewAdapter.VIEW_TYPE_PRODUCTS, list, getContext()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ErrorProduct", databaseError.toException().getMessage());
            }
        });

        return view;
    }

    @OnClick(R.id.floating_action_button)
    public void FloatingActionButton() {
        if (WOP.u.userType.equals("retailer")){

            Permissions.check(getContext(), new String[]{android.Manifest.permission.CAMERA},
                    "Camera and storage permissions are required because...",
                    new Permissions.Options()
                            .setSettingsDialogTitle("Warning!").setRationaleDialogTitle("Info"),
                    new PermissionHandler() {
                        @Override
                        public void onGranted() {
                            startActivityForResult(new Intent(getContext(), ScanQRCodeActivity.class), 1);
                        }
                    });


        }else
        startActivity(new Intent(getContext(), CreateProductActivity.class));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String code = data.getStringExtra("result");
                Log.e("QRCode", code);
                WOP.qrcode = code;
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.detach(WOP.p).attach(WOP.p).commit();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getContext(), "You have canceled the scan.", Toast.LENGTH_LONG)
                        .show();
            }

        }
    }


}
