package com.waveorderplacer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.waveorderplacer.R;
import com.waveorderplacer.WOP;
import com.waveorderplacer.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfileFragment extends Fragment {

    @BindView(R.id.mName)
    TextView mName;

    @BindView(R.id.mEmail)
    TextView mEmail;

    @BindView(R.id.mPhone)
    TextView mPhone;

    @BindView(R.id.mCompany)
    TextView mCompany;

    @BindView(R.id.mCountry)
    TextView mCountry;

    @BindView(R.id.mCity)
    TextView mCity;

    @BindView(R.id.mStreetNo)
    TextView mStreetNo;

    @BindView(R.id.mShopNo)
    TextView mShopNo;

    @BindView(R.id.mTown)
    TextView mTown;

    @BindView(R.id.avatar)
    CircularImageView mImage;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, v);

        User u = WOP.u;
        if (u != null) {
            Picasso.with(getContext()).load(WOP.u.pic).placeholder(R.drawable.default_avatar).into(mImage);
            mName.setText(String.valueOf(u.fname + " " + u.lname));
            mEmail.setText(String.valueOf(u.email));
            mPhone.setText(String.valueOf(u.contact));
            mCompany.setText(String.valueOf(u.companyShopName));
            mCountry.setText(String.valueOf(u.country));
            mCity.setText(String.valueOf(u.city));
            mStreetNo.setText(String.valueOf(u.streetNumber));
            mShopNo.setText(String.valueOf(u.shopPlotNumber));
            mTown.setText(String.valueOf(u.shopPlotNumber));
        }


        return v;
    }

}
