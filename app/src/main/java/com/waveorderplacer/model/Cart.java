package com.waveorderplacer.model;

import java.util.HashMap;

/**
 * Created by hussnain on 2/13/18.
 */

public class Cart {
    public String pid;
    public String name;
    public String category;
    public String size;
    public String method;

    public Cart() {

    }

    public Cart(String pid, String name, String category, String size, String method) {
        this.pid = pid;
        this.name = name;
        this.category = category;
        this.size = size;
        this.method = method;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("pid", pid);
        map.put("name", name);
        map.put("category", category);
        map.put("size", size);
        map.put("payment_method",method);

        return map;
    }
}
