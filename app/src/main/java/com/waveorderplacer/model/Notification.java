package com.waveorderplacer.model;

import java.util.HashMap;

/**
 * Created by ranatayyab on 2/12/18.
 */

public class Notification {
    public String name;
    public String message;
    public String product_name;
    public String product_quantity;
    public String notification_time;
    public String cell_no;

    public Notification() {
    }

    public Notification(String name, String message, String product_name, String product_quantity, String notification_time, String cell_no) {
        this.name = name;
        this.message = message;
        this.product_name = product_name;
        this.product_quantity = product_quantity;
        this.notification_time = notification_time;
        this.cell_no = cell_no;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("name", name);
        map.put("message", message);
        map.put("product_name",product_name);
        map.put("product_quantity",product_quantity);
        map.put("notification_time",notification_time);
        map.put("cell_no",cell_no);

        return map;
    }

   /* @Override
    public String toString() {
        return name + ", " + message + ", " + product_name + ", " + product_quantity + ", " + notification_time;
    }*/
}
