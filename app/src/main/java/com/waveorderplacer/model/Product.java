package com.waveorderplacer.model;

import java.util.HashMap;

/**
 * Created by ranatayyab on 2/12/18.
 */

public class Product {
    public String pid;
    public String qrcode;
    public String name;
    public String company;
    public String category;
    public String size;
    public String price;
    public String pic;

    public Product() {

    }

    public Product(String pid, String qrcode, String name, String company, String category, String size, String price, String pic) {
        this.pid = pid;
        this.qrcode = qrcode;
        this.name = name;
        this.company = company;
        this.category = category;
        this.size = size;
        this.price = price;
        this.pic = pic;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("pid", pid);
        map.put("qrcode", qrcode);
        map.put("name", name);
        map.put("company", company);
        map.put("category", category);
        map.put("size", size);
        map.put("price", price);
        map.put("picture", pic);

        return map;
    }
}
