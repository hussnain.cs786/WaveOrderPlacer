package com.waveorderplacer.model;

import java.util.HashMap;

/**
 * Created by ranatayyab on 2/12/18.
 */

public class User {
    public String uid;
    public String userType;
    public String fname;
    public String lname;
    public String contact;
    public String country;
    public String city;
    public String companyShopName;
    public String email;
    public String townColonyMarket;
    public String streetNumber;
    public String shopPlotNumber;
    public String pic;

    public User() {

    }

    public User(String uid, String userType, String fname, String lname, String contact, String country, String city, String companyShopName, String email, String townColonyMarket, String streetNumber, String shopPlotNumber, String pic) {
        this.uid = uid;
        this.userType = userType;
        this.fname = fname;
        this.lname = lname;
        this.contact = contact;
        this.country = country;
        this.city = city;
        this.companyShopName = companyShopName;
        this.email = email;
        this.townColonyMarket = townColonyMarket;
        this.streetNumber = streetNumber;
        this.shopPlotNumber = shopPlotNumber;
        this.pic = pic;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("uid", uid);
        map.put("userType", userType);
        map.put("fname", fname);
        map.put("lname", lname);
        map.put("contact", contact);
        map.put("country", country);
        map.put("city", city);
        map.put("companyShopName", companyShopName);
        map.put("email", email);
        map.put("townColonyMarket", townColonyMarket);
        map.put("streetNumber", streetNumber);
        map.put("shopPlotNumber", shopPlotNumber);
        map.put("picture", pic);


        return map;
    }
}
